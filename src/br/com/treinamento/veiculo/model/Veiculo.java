package br.com.treinamento.veiculo.model;

import br.com.treinamento.veiculo.enums.TipoVeiculo;

import java.math.BigDecimal;
import java.util.Date;

public abstract class Veiculo {

    private Integer id;
    private TipoVeiculo tipo;
    private String cor;
    private String modelo;
    private String combustivel;
    private Date dataDeFabricacao;
    private BigDecimal valorDeCusto;

    public Veiculo(){

    }

    public Veiculo(Integer id, TipoVeiculo tipo, String cor, String modelo, String combustivel, Date dataDeFabricacao, BigDecimal valorDeCusto) {
        this.id = id;
        this.tipo = tipo;
        this.cor = cor;
        this.modelo = modelo;
        this.combustivel = combustivel;
        this.dataDeFabricacao = dataDeFabricacao;
        this.valorDeCusto = valorDeCusto;
    }

    public BigDecimal valorDeVenda(Double porcentagem){
        Double porcento = porcentagem / 100;
        Double valor = (porcento * valorDeCusto.doubleValue()) + valorDeCusto.doubleValue();

        return BigDecimal.valueOf(valor);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoVeiculo getTipo() {
        return tipo;
    }

    public void setTipo(TipoVeiculo tipo) {
        this.tipo = tipo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public Date getDataDeFabricacao() {
        return dataDeFabricacao;
    }

    public void setDataDeFabricacao(Date dataDeFabricacao) {
        this.dataDeFabricacao = dataDeFabricacao;
    }

    public BigDecimal getValorDeCusto() {
        return valorDeCusto;
    }

    public void setValorDeCusto(BigDecimal valorDeCusto) {
        this.valorDeCusto = valorDeCusto;
    }

    @Override
    public String toString() {
        return  " tipo = " + tipo + "\n" +
                " cor = " + cor + "\n" +
                " modelo = " + modelo + "\n"  +
                " combustivel = " + combustivel + "\n"  +
                " DataDeFabricacao = " + dataDeFabricacao + "\n" +
                " valorDeCusto = " + valorDeCusto + "\n";
    }
}
