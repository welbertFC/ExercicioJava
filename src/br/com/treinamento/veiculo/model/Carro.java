package br.com.treinamento.veiculo.model;

import br.com.treinamento.veiculo.enums.TipoVeiculo;

import java.math.BigDecimal;
import java.util.Date;

public class Carro extends Veiculo{

    private Double potencia;
    private Integer portas;

    public Carro (){}

    public Carro(Integer id, TipoVeiculo tipo, String cor, String modelo, String combustivel, Date dataDeFabricacao, BigDecimal valorDeCusto, Double potencia, Integer portas) {
        super(id, tipo, cor, modelo, combustivel, dataDeFabricacao, valorDeCusto);
        this.potencia = potencia;
        this.portas = portas;
    }

    public Double getPotencia() {
        return potencia;
    }

    public void setPotencia(Double potencia) {
        this.potencia = potencia;
    }

    public Integer getPortas() {
        return portas;
    }

    public void setPortas(Integer portas) {
        this.portas = portas;
    }

    @Override
    public String toString() {
        return "Carro " + "\n" +
                super.toString() +
                " potencia = " + potencia + "\n" +
                " portas = " + portas + "\n" +
                " ";
    }
}
