package br.com.treinamento.veiculo.model;

import br.com.treinamento.veiculo.enums.TipoMotorAviao;
import br.com.treinamento.veiculo.enums.TipoVeiculo;

import java.math.BigDecimal;
import java.util.Date;

public class Aviao extends Veiculo {

    private Integer quantidadeDeMotores;
    private TipoMotorAviao tipoMotorAviao;


    public Aviao (){

    }

    public Aviao(Integer id, TipoVeiculo tipo, String cor, String modelo, String combustivel, Date dataDeFabricacao, BigDecimal valorDeCusto, Integer quantidadeDeMotores, TipoMotorAviao tipoMotorAviao) {
        super(id, tipo, cor, modelo, combustivel, dataDeFabricacao, valorDeCusto);
        this.quantidadeDeMotores = quantidadeDeMotores;
        this.tipoMotorAviao = tipoMotorAviao;

    }

    public Integer getQuantidadeDeMotores() {
        return quantidadeDeMotores;
    }

    public void setQuantidadeDeMotores(Integer quantidadeDeMotores) {
        this.quantidadeDeMotores = quantidadeDeMotores;
    }

    public TipoMotorAviao getTipoMotorAviao() {
        return tipoMotorAviao;
    }

    public void setTipoMotorAviao(TipoMotorAviao tipoMotorAviao) {
        this.tipoMotorAviao = tipoMotorAviao;
    }


    public String altitudeMaxima(){
        if( this.quantidadeDeMotores == 1){
            System.out.println("Altitude maxima de 15 mil pés");
        }else if (this.quantidadeDeMotores == 2){
            System.out.println("Altitude maxima de 20 mil pés");
        }else if (this.quantidadeDeMotores == 3){
            System.out.println("Altitude maxima de 30 mil pés");
        }else if (this.quantidadeDeMotores == 4){
            System.out.println("Altitude maxima de 50 mil pés");
        }else{
           System.out.println("Numero de motores invalido");
        }
        return "Numero de motores: " + quantidadeDeMotores.toString();
    }

    @Override
    public String toString() {
        return "Avião " + "\n" +
                super.toString() +
                " motor = " + tipoMotorAviao.getDescricao() + "\n" +
                " numero de motores = " + quantidadeDeMotores+ "\n" +
                " ";
    }
}
