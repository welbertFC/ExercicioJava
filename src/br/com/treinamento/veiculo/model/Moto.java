package br.com.treinamento.veiculo.model;

import br.com.treinamento.veiculo.enums.TipoVeiculo;

import java.math.BigDecimal;
import java.util.Date;

public class Moto extends Veiculo {

    private Double cilindradas;
    private String classificacao;


    public Moto() {

    }

    public Moto(Integer id, TipoVeiculo tipo, String cor, String modelo, String combustivel, Date dataDeFabricacao, BigDecimal valorDeCusto, Double cilindradas, String classificacao) {
        super(id, tipo, cor, modelo, combustivel, dataDeFabricacao, valorDeCusto);
        this.cilindradas = cilindradas;
        this.classificacao = classificacao;
    }

    public Double getCilindradas() {
        return cilindradas;
    }

    public void setCilindradas(Double cilindradas) {
        this.cilindradas = cilindradas;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public String toString() {
        return "Moto " + "\n" +
                super.toString() +
                " cilindradas = " + cilindradas +
                " classificação = " + classificacao +
                " ";
    }
}
