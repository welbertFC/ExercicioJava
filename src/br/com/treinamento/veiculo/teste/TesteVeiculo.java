package br.com.treinamento.veiculo.teste;

import br.com.treinamento.veiculo.enums.TipoMotorAviao;
import br.com.treinamento.veiculo.enums.TipoVeiculo;
import br.com.treinamento.veiculo.model.Aviao;
import br.com.treinamento.veiculo.model.Carro;
import br.com.treinamento.veiculo.model.Moto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TesteVeiculo {

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Moto moto = new Moto(null, TipoVeiculo.TERRESTRE, "preto", "MT-09", "Gasolina",
                formato.parse("20/05/2020"), BigDecimal.valueOf(45000.00), 900.0, "Naked");

        Carro carro = new Carro(null, TipoVeiculo.TERRESTRE, "vermelho", "Ferrari Spider", "Gasolina",
                formato.parse("20/05/2015"),BigDecimal.valueOf(4000000.0), 3.9, 2);

        Aviao aviao = new Aviao(null, TipoVeiculo.AEREO, "branco", "cessna 172", "Querosene de aviação",
                formato.parse("10/05/200"), BigDecimal.valueOf(197000.0), 3, TipoMotorAviao.PISTAO);



        System.out.println(" valor de venda : " + moto.valorDeVenda(12.5) + "\n" + moto + "\n");

        System.out.println(" valor de venda : " + carro.valorDeVenda(12.0) + "\n" + carro + "\n");

        System.out.println(" valor de venda : " + aviao.valorDeVenda(7.8) + "\n" + aviao + "\n");

        System.out.println(aviao.altitudeMaxima());





    }
}
