package br.com.treinamento.veiculo.enums;

public enum TipoVeiculo {

    AGUATICO(1, "Veiculo Aquático "),
    TERRESTRE(2, "Veiculo Terrestre"),
    AEREO(3, "Veiculo Aéreo");

    private Integer codigo;
    private String descricao;

    TipoVeiculo(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    
}
