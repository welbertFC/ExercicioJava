package br.com.treinamento.veiculo.enums;

public enum TipoMotorAviao {

    TURBOHELICE(1, "Motor turbo hélice"),
    TURBOFAN(2, "Motor turbo Fan"),
    PISTAO(3, "Motor a pistão");

    private Integer codigo;
    private String descricao;

    TipoMotorAviao(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }
}
